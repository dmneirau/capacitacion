<?php
 
namespace Intcomex\Preguntas\Model\ResourceModel\Preguntas;
 
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
 
class Collection extends AbstractCollection
{
    protected function _construct()
    {
        $this->_init(
            'Intcomex\Preguntas\Model\Preguntas',
            'Intcomex\Preguntas\Model\ResourceModel\Preguntas'
        );
    }
}