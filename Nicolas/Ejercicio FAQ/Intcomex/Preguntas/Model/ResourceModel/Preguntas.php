<?php
 
namespace Intcomex\Preguntas\Model\ResourceModel;
 
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
 
class Preguntas extends AbstractDb
{
    protected function _construct()
    {
        $this->_init('preguntas_frecuentes', 'id');
    }
}