<?php
 
namespace Intcomex\Preguntas\Model;
 
use Magento\Framework\Model\AbstractModel;
 
class Preguntas extends AbstractModel
{
    protected function _construct()
    {
        $this->_init('Intcomex\Preguntas\Model\ResourceModel\Preguntas');
    }
}