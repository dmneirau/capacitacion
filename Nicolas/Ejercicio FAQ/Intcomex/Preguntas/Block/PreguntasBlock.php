<?php
namespace Intcomex\Preguntas\Block;

use Magento\Framework\App\RequestInterface;
use Intcomex\Preguntas\Model\Preguntas;

class PreguntasBlock extends \Magento\Framework\View\Element\Template 
{
    protected $preguntasModelFactory;
    protected $request;

    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        Preguntas $preguntasModelFactory,
        RequestInterface $request,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_preguntasModelFactory = $preguntasModelFactory;
        $this->_request = $request;
    }

    protected function _prepareLayout() {
        parent::_prepareLayout();
        $this->pageConfig->getTitle()->set(__('Preguntas Frecuentes'));
        return $this;
    }

    public function getPreguntasData()
    {
        $collection = $this->_preguntasModelFactory->getCollection();
        return $collection;
    }
}