<?php

namespace Intcomex\Preguntas\Controller\Custom;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Intcomex\Preguntas\Model\PreguntasFactory;
use Magento\Framework\View\Result\PageFactory;

class Preguntas extends \Magento\Framework\App\Action\Action
{

    protected $resultJsonFactory;

    protected $_modelPreguntasFactory;

    protected $resultPageFactory;
   
    public function __construct(
        Context $context, 
        PreguntasFactory $_modelPreguntasFactory,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        PageFactory $resultPageFactory
    ) 
    {
        parent::__construct($context);
        $this->resultJsonFactory = $resultJsonFactory;
        $this->_modelPreguntasFactory = $_modelPreguntasFactory;
        $this->resultPageFactory = $resultPageFactory;
    }

    public function execute()
    {
        $this->_view->loadLayout();
        $this->_view->renderLayout();
        
    }

}