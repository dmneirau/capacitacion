Ruta
intcomex/custom/preguntas

Ejecutar las siguientes lineas
chmod 0777 -R var/ generated/ pub/
php bin/magento setup:upgrade
bin/magento  setup:db-schema:upgrade
php bin/magento setup:di:compile
bin/magento module:enable --clear-static-content Intcomex_Preguntas
chmod 0777 -R var/ generated/ pub/

Ejecutar query en bdd
INSERT INTO preguntas_frecuentes VALUES 
(NULL,"Como logearse en la tienda","haciendo click en iniciar session",NOW(),NOW()),
(NULL,"Como Recuperar la contraseña","haciendo click en recuperar contraseña en la página de login",NOW(),NOW()),
(NULL,"Que garantia tienen los productos","La garantia esta descrita en cada producto",NOW(),NOW()),
(NULL,"Como devolver un producto","Enviarlo a la direcion: Av Miami Cl 34",NOW(),NOW()),
(NULL,"Telefonos de contacto ","57319285794 en colombia, 12031212 en Miami",NOW(),NOW());
