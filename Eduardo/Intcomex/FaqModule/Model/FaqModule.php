<?php
namespace Intcomex\FaqModule\Model;
class FaqModule extends \Magento\Framework\Model\AbstractModel
{
    protected function _construct()
    {
        $this->_init(\Intcomex\FaqModule\Model\ResourceModel\FaqModule::class);
    }
}
