<?php
namespace Intcomex\FaqModule\Model\ResourceModel\FaqModule;
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init(\Intcomex\FaqModule\Model\FaqModule::class,
            \Intcomex\FaqModule\Model\ResourceModel\FaqModule::class);
    }
}
