<?php
namespace Intcomex\FaqModule\Model\ResourceModel;
class FaqModule extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('faquest','faq_id');
    }
}
