<?php



namespace Intcomex\FaqModule\Controller\Index;



use Magento\Framework\App\Action\Action;

use Magento\Framework\App\Action\Context;

use Intcomex\FaqModule\Model\FaqModuleFactory;



class Index extends \Magento\Framework\App\Action\Action

{



    /**

     * @var \Magento\Framework\Controller\Result\JsonFactory

     */

    protected $resultJsonFactory;



    protected $_modelFaqFactory;



    public function __construct(

        Context $context,

        FaqModuleFactory $modelFaqFactory,

        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory

    ) {

        parent::__construct($context);

        $this->resultJsonFactory = $resultJsonFactory;

        $this->_modelFaqFactory = $modelFaqFactory;

    }



    /**

     * Execute view action

     *

     * @return \Magento\Framework\Controller\ResultInterface

     */

    public function execute(){



        $resultPage = $this->_modelFaqFactory->create();

        $collection = $resultPage->getCollection(); //Get Collection of module data

        echo '<pre>';

        print_r($collection->getData());

        echo '</pre>';

        exit;



    }



}
