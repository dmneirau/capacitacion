<?php
namespace Ejercicio\Tabla\Controller\Index;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;

class Index extends Action
{
    /**
     * @var \Ejercicio\Tabla\Model\TablaFactory
     */
    private $tablaModelFactory;
    /**
     * @var \Ejercicio\Tabla\Model\ResourceModel\Tabla
     */
    private $tablaResourceModel;
    /**
     * @var \Ejercicio\Tabla\Model\ResourceModel\Tabla\CollectionFactory
     */
    private $tablaCollectionFactory;

    public function __construct(Context $context,
                                \Ejercicio\Tabla\Model\TablaFactory $tablaModelFactory,
                                \Ejercicio\Tabla\Model\ResourceModel\Tabla $tablaResourceModel,
                                \Ejercicio\Tabla\Model\ResourceModel\Tabla\CollectionFactory $tablaCollectionFactory)
    {
        parent::__construct($context);
        $this->tablaModelFactory = $tablaModelFactory;
        $this->tablaResourceModel = $tablaResourceModel;
        $this->tablaCollectionFactory = $tablaCollectionFactory;
    }
    public function execute()
    {
        // TODO: Implement execute() method.
        //$this->agregarInformacion();
        $this->mostrarInformacion();
    }
    protected function agregarInformacion()
    {
        /** @var \Ejercicio\Tabla\Model\Tabla $newTabla */
        $newTabla = $this->tablaModelFactory->create();
        $newTabla->setData('preguntas','¿Cual es el comando para actualizar datos en la base de datos?')
            ->setData('respuestas','bin/magento setup:db-data:upgrade')
            ->setData('customer_id',1);
        $this->tablaResourceModel->save($newTabla);
        $newData = $newTabla->getData();
        var_export($newData);
    }
    protected function mostrarInformacion()
    {
        /** @var \Ejercicio\Tabla\Model\ResourceModel\Tabla\Collection $collection */
        $collection = $this->tablaCollectionFactory->create();
        $collection->addFieldToSelect(['preguntas','respuestas'])
            ->setCurPage(1)
            ->setOrder('tabla_id','asc');
        $data = $collection->getData();
        echo "<pre>";
        var_export($data);
        echo "<pre>";
    }
}
