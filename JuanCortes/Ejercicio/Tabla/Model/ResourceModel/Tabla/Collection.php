<?php
namespace Ejercicio\Tabla\Model\ResourceModel\Tabla;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
class Collection extends AbstractCollection
{
    protected function _construct()
    {
        $this->_init(\Ejercicio\Tabla\Model\Tabla::class,\Ejercicio\Tabla\Model\ResourceModel\Tabla::class);
    }
}
