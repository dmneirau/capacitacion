<?php
namespace Ejercicio\Tabla\Model\ResourceModel;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
class Tabla extends AbstractDb
{
    protected function _construct()
    {
        $this->_init('tabla','tabla_id');
    }
}
