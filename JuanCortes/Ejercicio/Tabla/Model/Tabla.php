<?php
namespace Ejercicio\Tabla\Model;
use Magento\Framework\Model\AbstractModel;
class Tabla extends AbstractModel
{
    protected function _construct()
    {
        $this->_init(\Ejercicio\Tabla\Model\ResourceModel\Tabla::class);
    }
}
