<?php

namespace EjercicioFinal\TablaFAQ\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultFactory;

class Index extends Action
{
    /**
     * @var \EjercicioFinal\TablaFAQ\Model\TablaFAQFactory
     */
    private $tablaFAQModelFactory;
    /**
     * @var \EjercicioFinal\TablaFAQ\Model\ResourceModel\TablaFAQ
     */
    private $tablaFAQResourceModel;
    /**
     * @var \EjercicioFinal\TablaFAQ\Model\ResourceModel\TablaFAQ\CollectionFactory
     */
    private $tablaFAQCollectionFactory;

    public function __construct(Context $context, \EjercicioFinal\TablaFAQ\Model\TablaFAQFactory $tablaFAQModelFactory, \EjercicioFinal\TablaFAQ\Model\ResourceModel\TablaFAQ $tablaFAQResourceModel, \EjercicioFinal\TablaFAQ\Model\ResourceModel\TablaFAQ\CollectionFactory $tablaFAQCollectionFactory)
    {
        parent::__construct($context);
        $this->tablaFAQModelFactory = $tablaFAQModelFactory;
        $this->tablaFAQResourceModel = $tablaFAQResourceModel;
        $this->tablaFAQCollectionFactory = $tablaFAQCollectionFactory;
    }

    public function execute()
    {
        /*$pageResult = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $pageResult->getConfig()->getTitle()->set('Tabla FAQ');
        return $pageResult;*/
        //$this->AgregarPreguntasYRespuestas();
        $this->ListarPreguntasYRespuestas();
    }

    protected function AgregarPreguntasYRespuestas()
    {
        /** @var \EjercicioFinal\TablaFAQ\Model\TablaFAQ $newFAQ */
        for($i = 1; $i <= 9; $i++)
        {
            $newFAQ = $this->tablaFAQModelFactory->create();
            if($i == 0)
            {
                $newFAQ->setData('pregunta','¿Qué es Magento?')
                       ->setData('respuesta','Magento es posiblemente la plataforma de comercio electrónico de código abierto más utilizada en el mercado, proporcionando a los comerciantes una plataforma de compras en línea muy escalable, flexible e intuitiva. En detalle, presenta una variedad de herramientas sofisticadas para marketing en línea, SEO, gestión de clientes, facturación y gestión de inventario, que puede personalizar según sus necesidades individuales. En consecuencia, la plataforma Magento se considera la mejor opción de solución de comercio electrónico para tiendas en línea a gran escala que buscan una interfaz de usuario totalmente personalizada con muchas características personalizadas.')
                       ->setData('customer_id',1);
            }
            else if($i == 1)
            {
                $newFAQ->setData('pregunta','¿Cual es el beneficio de Open Source?')
                    ->setData('respuesta','Las plataformas de código abierto permiten a los usuarios tener acceso directo al código fuente, lo que permite a los desarrolladores modificarlo según sus requisitos específicos. Al hacerlo, permiten a los propietarios de negocios la libertad de alojar el software en el proveedor de alojamiento que deseen, lo que significa que usted dueño de su sitio web. En vista de esto, no alquile su sitio web, que es lo que ofrecen muchas de las plataformas de bricolaje (Square Space, Wix, Weebly, Shopify, etc.). Si estas plataformas de bricolaje cierran, también lo hará su sitio web.')
                    ->setData('customer_id',1);
            }
            else if($i == 2)
            {
                $newFAQ->setData('pregunta','¿Cuánto cuesta Magento?')
                    ->setData('respuesta','Existe una versión comunitaria gratuita de Magento, que es adecuada para la mayoría de las pequeñas tiendas en línea. Sin embargo, los sitios empresariales de mayor escala requerirían un nivel de servicio diferente.')
                    ->setData('customer_id',1);
            }
            else if($i == 3)
            {
                $newFAQ->setData('pregunta','¿Qué proveedores de pasarela de pago puedo utilizar con Magento?')
                    ->setData('respuesta','Hay muchos proveedores de pasarelas de pago que trabajan con Magento. Podemos integrar cualquier proveedor de pago que utilice actualmente. ')
                    ->setData('customer_id',1);
            }
            else if($i == 4)
            {
                $newFAQ->setData('pregunta','¿Cómo puedo encontrar el mejor soporte para Magento?')
                    ->setData('respuesta','Finalmente, Magento puede ser una plataforma complicada para desarrolladores web que no la han usado antes. Sugerimos encontrar a alguien que tenga un historial probado en la construcción de tiendas Magento personalizadas en el pasado. ')
                    ->setData('customer_id',1);
            }
            else if($i == 5)
            {
                $newFAQ->setData('pregunta','¿Magento se considera una plataforma mejor que Shopify?')
                    ->setData('respuesta','Tanto Magento como Shopify son herramientas poderosas para construir una plataforma de comercio electrónico. Si bien Shopify es un software pago, Magento es un software de código abierto accesible para cualquier persona. Sin embargo, Magento requiere conocimientos previos de codificación, mientras que Shopify está fácilmente disponible para los usuarios que buscan construir su plataforma fácilmente.')
                    ->setData('customer_id',1);
            }
            else if($i == 6)
            {
                $newFAQ->setData('pregunta','¿Magento se considera una plataforma mejor que WooCommerce?')
                    ->setData('respuesta','Mientras que Magento y WooCommerce son software de código abierto gratuito, los costos de construir y administrar una plataforma de comercio electrónico basada en Magento pueden ser mucho más altos que los de la plataforma WooCommerce. Al mismo tiempo, los usuarios prefieren emplear WooComerce debido a su alta compatibilidad con los sitios web de WordPress, el sistema de administración de contenido más poderoso del mundo.')
                    ->setData('customer_id',1);
            }
            else if($i == 7)
            {
                $newFAQ->setData('pregunta','¿Magento se considera una plataforma mejor que Opencart?')
                    ->setData('respuesta','Opencart está diseñado principalmente para empresas de aficionados, mientras que la plataforma de Magento está destinada a empresas más grandes que pueden soportar los costos más altos y la curva de aprendizaje. La amplia comunidad de Magento en comparación con la de la plataforma Opencart menos popular también hace que Magento sea más atractivo ya que los usuarios de Magento tienen un acceso más fácil al soporte.')
                    ->setData('customer_id',1);
            }
            else if($i == 8)
            {
                $newFAQ->setData('pregunta','¿Magento se considera una plataforma mejor que PrestaShop?')
                    ->setData('respuesta','La comunidad más grande de usuarios de Magento, además del equipo de soporte de pago, asegura a los usuarios que pueden encontrar dificultades al usar el software; mientras tanto PrestaShops el soporte es relativamente menor. La infraestructura de Magento admite empresas más complejas y de gran escala, mientras que, similar a las otras plataformas, PrestaShop es perfectamente adecuada para tiendas más pequeñas.')
                    ->setData('customer_id',1);
            }
            else if($i == 9)
            {
                $newFAQ->setData('pregunta','¿Cómo instalo Magento?')
                    ->setData('respuesta','En comparación con la mayoría de las plataformas de comercio electrónico, la instalación de Magento se convierte en un proceso bastante complicado sin la orientación adecuada. Para instalar Magento, los usuarios deben configurar el entorno de desarrollo (sistema operativo, servidor web, PHP, base de datos MySQL). Los usuarios también deben descargar el Software Magento y utilice la línea de comandos y el asistente de configuración para la instalación. Existe una opción para descargar con o sin datos de muestra, que está diseñada para familiarizar a los usuarios con la plataforma Magento.')
                    ->setData('customer_id',1);
            }
            $this->tablaFAQResourceModel->save($newFAQ);
            $newFAQData = $newFAQ->getData();
            var_export($newFAQData);
        }

    }
    protected function ListarPreguntasYRespuestas()
    {
        /** @var \EjercicioFinal\TablaFAQ\Model\ResourceModel\TablaFAQ\Collection $collection */
        $collection = $this->tablaFAQCollectionFactory->create();
        $collection->addFieldToSelect(['pregunta','respuesta'])->setPageSize(10)->setCurPage(1)->setOrder('tablaFAQ_id','desc');
        $data = $collection->getData();
        echo "<pre>";
        var_export($data);
        echo "<pre>";
    }
}
