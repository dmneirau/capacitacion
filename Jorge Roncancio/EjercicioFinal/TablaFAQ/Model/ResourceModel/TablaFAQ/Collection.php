<?php

namespace EjercicioFinal\TablaFAQ\Model\ResourceModel\TablaFAQ;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected function _construct()
    {
        $this->_init(\EjercicioFinal\TablaFAQ\Model\TablaFAQ::class,\EjercicioFinal\TablaFAQ\Model\ResourceModel\TablaFAQ::class);
    }
}
