<?php

namespace EjercicioFinal\TablaFAQ\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class TablaFAQ extends AbstractDb
{
    protected function _construct()
    {
        $this->_init('tablaFAQ','tablaFAQ_id');
    }
}
