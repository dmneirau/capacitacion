<?php

namespace EjercicioFinal\TablaFAQ\Model;

use Magento\Framework\Model\AbstractModel;

class TablaFAQ extends AbstractModel
{
    protected function _construct()
    {
        $this->_init(\EjercicioFinal\TablaFAQ\Model\ResourceModel\TablaFAQ::class);
    }
}
