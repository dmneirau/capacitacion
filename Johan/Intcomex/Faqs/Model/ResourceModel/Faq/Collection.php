<?php
 
namespace Intcomex\Faqs\Model\ResourceModel\Faq;
 
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
 
class Collection extends AbstractCollection
{
    /**
     * Define model & resource model
     */
    protected function _construct()
    {
        $this->_init(
            'Intcomex\Faqs\Model\Faq',
            'Intcomex\Faqs\Model\ResourceModel\Faq'
        );
    }
}