<?php
 
namespace Intcomex\Faqs\Model;
 
use Magento\Framework\Model\AbstractModel;
 
class Faq extends AbstractModel
{
    protected function _construct()
    {
        $this->_init('Intcomex\Faqs\Model\ResourceModel\Faq');
    }
}