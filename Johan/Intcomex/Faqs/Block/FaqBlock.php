<?php
namespace Intcomex\Faqs\Block;

use Magento\Framework\App\RequestInterface;
use Intcomex\Faqs\Model\Faq;

class FaqBlock extends \Magento\Framework\View\Element\Template 
{
    protected $faqModelFactory;
    protected $request;

    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        Faq $faqModelFactory,
        RequestInterface $request,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_faqModelFactory = $faqModelFactory;
        $this->_request = $request;
    }

    /**
     * Preparing global layout
     *
     * @return $this
     */
    protected function _prepareLayout() {
        parent::_prepareLayout();
        $this->pageConfig->getTitle()->set(__('Preguntas Frecuentes'));
        return $this;
    }

    public function getFaqData()
    {
        $collection = $this->_faqModelFactory->getCollection();
        return $collection;
    }
}