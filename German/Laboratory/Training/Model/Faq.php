<?php
 
namespace Intcomex\Training\Model;
 
use Magento\Framework\Model\AbstractModel;
 
class Faq extends AbstractModel
{
    protected function _construct()
    {
        $this->_init('Laboratory\Training\Model\ResourceModel\Faq');
    }
}