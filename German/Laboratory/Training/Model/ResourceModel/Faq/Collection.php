<?php
 
namespace Laboratory\Training\Model\ResourceModel\Faq;
 
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
 
class Collection extends AbstractCollection
{
    /**
     * Define model & resource model
     */
    protected function _construct()
    {
        $this->_init(
            'Laboratory\Training\Model\Faq',
            'Laboratory\Training\Model\ResourceModel\Faq'
        );
    }
}