<?php
 
namespace Intcomex\Faqs\Model\ResourceModel;
 
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
 
class Faq extends AbstractDb
{
    protected function _construct()
    {
        $this->_init('faqs', 'id'); 
    }
}