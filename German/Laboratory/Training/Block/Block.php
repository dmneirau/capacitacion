<?php
namespace Laboratory\Training\Block;

use Magento\Framework\App\RequestInterface;
use Laboratory\Training\Model\Faq;

class Block extends \Magento\Framework\View\Element\Template 
{
    protected $_modelFactory;
    protected $_request;

    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        Faq $modelFactory,
        RequestInterface $request,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_modelFactory = $modelFactory;
        $this->_request = $request;
    }

    /**
     * Preparing global layout
     *
     * @return $this
     */
    protected function _prepareLayout() {
        parent::_prepareLayout();
        $this->pageConfig->getTitle()->set(__('Frequent questions'));
        return $this;
    }

    public function getFaqs()
    {
        $collection = $this->_modelFactory->getCollection();
        return $collection;
    }
}