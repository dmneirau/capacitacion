<?php

namespace Laboratory\Training\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Laboratory\Training\Model\FaqFactory;
use Magento\Framework\View\Result\PageFactory;

class Faq extends \Magento\Framework\App\Action\Action
{

    /**
    * @var \Magento\Framework\Controller\Result\JsonFactory
    */
    protected $_resultJsonFactory;

    protected $_modelFaqFactory;

    protected $_resultPageFactory;
   
    public function __construct(
        Context $context, 
        FaqFactory $modelFaqFactory,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->_resultJsonFactory = $resultJsonFactory;
        $this->_modelFaqFactory   = $modelFaqFactory;
        $this->_resultPageFactory = $resultPageFactory;
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute(){ 

        // render layout
        $this->_view->loadLayout();
        $this->_view->renderLayout();
        
    }

}