<?php
 
namespace Intcomex\Faq\Model\ResourceModel;
 
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
 
class Faq extends AbstractDb
{
    /**
     * Define main table
     */
    protected function _construct()
    {
        $this->_init('faq_questions', 'id'); //hello is table of module
    }
}