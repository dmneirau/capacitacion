<?php
 
namespace Intcomex\Faq\Model\ResourceModel\Faq;
 
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
 
class Collection extends AbstractCollection
{
    /**
     * Define model & resource model
     */
    protected function _construct()
    {
        $this->_init(
            'Intcomex\Faq\Model\Faq',
            'Intcomex\Faq\Model\ResourceModel\Faq'
        );
    }
}