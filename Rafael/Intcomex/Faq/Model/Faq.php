<?php
 
namespace Intcomex\Faq\Model;
 
use Magento\Framework\Model\AbstractModel;
 
class Faq extends AbstractModel
{
    /**
     * Define resource model
     */
    protected function _construct()
    {
        $this->_init('Intcomex\Faq\Model\ResourceModel\Faq');
    }
}