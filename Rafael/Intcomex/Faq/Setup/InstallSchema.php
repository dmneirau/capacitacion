<?php

namespace Intcomex\Faq\Setup;
use Magento\Framework\App\ResourceConnection;

class InstallSchema implements \Magento\Framework\Setup\InstallSchemaInterface
{
	protected $resourceConnection;

	public function __construct(ResourceConnection $resourceConnection)
    {
        $this->resourceConnection = $resourceConnection;
    }

	public function install(\Magento\Framework\Setup\SchemaSetupInterface $setup, \Magento\Framework\Setup\ModuleContextInterface $context)
	{
		$connection = $this->resourceConnection->getConnection();
		$table = $connection->getTableName('faq_questions');
        $query = 'INSERT INTO '.$table.' VALUES (NULL,"Como logearse en la tienda","haciendo click en iniciar session","'.date('Y-m-d H:i:s').'","'.date('Y-m-d H:i:s').'"),';
		$query .= '(NULL,"Como Recuperar la contraseña","haciendo click en recuperar contraseña en la página de login","'.date('Y-m-d H:i:s').'","'.date('Y-m-d H:i:s').'"),';
		$query .= '(NULL,"Que garantia tienen los productos","La garantia esta descrita en cada producto","'.date('Y-m-d H:i:s').'","'.date('Y-m-d H:i:s').'"),';
		$query .= '(NULL,"Como devolver un producto","Enviarlo a la direcion: Av Miami Cl 34","'.date('Y-m-d H:i:s').'","'.date('Y-m-d H:i:s').'"),';
		$query .= '(NULL,"Telefonos de contacto ","57319285794 en colombia, 12031212 en Miami","'.date('Y-m-d H:i:s').'","'.date('Y-m-d H:i:s').'")';
        $connection->query($query);
	}
}